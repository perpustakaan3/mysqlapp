# mysqlapp
mysqlapp merupakan aplikasi microservice yang bertanggung jawab untuk mengatur database customer dan peminjaman perpustakaan  <br>
**Setup**<br>
Port menggunakan default dari Flask, yaitu port 5000<br>
**Dokumentasi API**<br>
- Customer<br>
1. Mengambil data semua customer<br>
**GET** `localhost:5000/users`<br>
2. Mengambil data token berdasarkan customer<br>
``
**GET** `localhost:5000/user/requesttoken`<br>
    - Body <br>
            - raw JSON <br> 
                ```
                {
                    "emai":"_email customer_"
                }
                ```

3. Menambahkan customer<br>
**POST** `localhost:5000/user/insert`<br>
    - Body <br>
            - raw JSON <br> 
                ```
                {
                    "username":"_username customer_", 
                    "namadepan":"_nama depan customer_", 
                    "namabelakang":"_nama belakang customer_", 
                    "email":"_email customer_"
                }
                ```

4. Mengubah data customer<br>
**POST** `localhost:5000/user/update`<br>
    - Body <br>
            - raw JSON <br> 
                ```
                {
                    "userid":"_id customer yang ingin diubah_",
                    "username":"_username customer_", 
                    "namadepan":"_nama depan customer_", 
                    "namabelakang":"_nama belakang customer_", 
                    "email":"_email customer_"
                }
                ```
5. Menghapus data customer<br>
**POST** `localhost:5000/user/delete`<br>
    - Body <br>
            - raw JSON <br> 
                ```
                {
                    "userid":"_id customer yang ingin dihapus_",
                }
                ```
- Peminjaman <br>
1. Melihat data peminjaman sesuai dengan token customer<br>
**GET** `localhost:5000/borrows`<br>
    - Authorization -> Pilih type Bearer Token -> Masukkan token yang didapat dari API Mengambil data token berdasarkan customer<br>
2. Menambahkan data peminjaman sesuai dengan token customer <br>
**POST** `localhost:5000/borrows/insert`<br>
    - Authorization -> Pilih type Bearer Token -> Masukkan token yang didapat dari API Mengambil data token berdasarkan customer<br>
    - Body <br>
            - raw JSON <br> 
                ```
                {
                    "bookid":"_bookid buku yang dipinjam_",
                }
                ```
3. Mengubah status peminjaman menjadi 0 sesuai dengan token customer dan id peminjaman<br>
**POST** `localhost:5000/borrows/status`<br>
    - Authorization -> Pilih type Bearer Token -> Masukkan token yang didapat dari API Mengambil data token berdasarkan customer<br>
    - Body <br>
            - raw JSON <br> 
                ```
                {
                    "borrowid":"_borrowid peminjaman yang ingin diubah statusnya_",
                }
                ```
