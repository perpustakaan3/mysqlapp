from mysql.connector import connect


class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='tugas_akhir',
                              user='root',
                              password='')
        except Exception as e:
            print(e)

    def showUsers(self):
        try:
            crud_query = '''
            SELECT * 
            FROM customers;
            '''
            cursor = self.db.cursor()
            cursor.execute(crud_query)
            users = cursor.fetchall()
            return (users)
        except Exception as e:
            print(e)

    def showUserById(self, **params):
        try:
            userid = dataId
            crud_query = '''
            SELECT * 
            FROM customers 
            WHERE userid = {0};
            '''.format(params["userid"])
            cursor = self.db.cursor()
            cursor.execute(crud_query)
            users = cursor.fetchall()
            return (users)
        except Exception as e:
            print(e)

    def showUserByEmail(self, **params):
        cursor = self.db.cursor()
        query = '''
        SELECT * 
        FROM customers 
        WHERE email = "{0}";
        '''.format(params["email"])
        cursor.execute(query)
        result = cursor.fetchone()
        return result

    def insertUser(self, **params):
        column = ", ".join(list(params.keys()))
        values = tuple(list(params.values()))
        cursor = self.db.cursor()
        query = '''
        INSERT INTO customers ({0})
        VALUES{1};
        '''.format(column, values)
        cursor.execute(query)

    def updateUserById(self, **params):
        try:
            userid = params['userid']
            values = self.restructureParams(**params)
            crud_query = '''
            UPDATE customers 
            SET {0} 
            WHERE userid = {1};
            '''.format(values, userid)
            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)

    def deleteUserById(self, **params):
        try:
            userid = params['userid']
            crud_query = '''
            DELETE FROM customers 
            WHERE userid = {0};
            '''.format(userid)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)

    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(
            item[0], item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
