from app import app
from app.controllers import customers_controller
from flask import Blueprint, request

customer_blueprint = Blueprint("customer_router", __name__)


@app.route("/users", methods=["GET"])
def showUsers():
    return customers_controller.shows()


@app.route("/user/insert", methods=["POST"])
def insertUsers(**params):
    params = request.json
    return customers_controller.insert(**params)


@app.route("/user/update", methods=["POST"])
def updateUsers(**params):
    params = request.json
    return customers_controller.update(**params)


@app.route("/user/delete", methods=["POST"])
def deleteUsers(**params):
    params = request.json
    return customers_controller.delete(**params)


@app.route("/user/requesttoken", methods=["GET"])
def requesttoken():
    params = request.json
    return customers_controller.token(**params)
